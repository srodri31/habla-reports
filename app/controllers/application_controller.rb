class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # before_action :ensure_credentials
  # before_action :set_analytics, only: [:ruby]
  # after_action :after

  require 'googleauth'
  require 'google/apis/analytics_v3'

  require 'koala'

## facebook

  def implementation_fbapi
    FacebookAds.configure do |config|
      config.access_token = 'EAAS9Tripd30BAINdDLZBstOM1GwbmvANzlGfgFY6KecSQixmCi9GkfYTyywF1vtvGEB5YBxV6XMpq7TUBjyIOGkphm0SdVsq6CC75F3oP1ZBzyVVEhCdIgQpy1sU66xjHO59Ng187TxhGq9f0lEcMOZCsMeELMdknZB5fHD6xgZDZD'
      config.app_secret = 'f81516205be0682015e36e931f387c08'
    end
    @names = Array.new
    act_accounts = FacebookAds.me.adaccounts.to_a.map { |x| x['id']}.take(5)
    act_accounts.each do |account|
      temp_name = FacebookAds::AdAccount.get(account)
      @names << [temp_name.name, account]
    end
  end

  def get_value
    @value =  Array.new
    begin
      account_id =  params[:id]
      account = FacebookAds::AdAccount.get(account_id)
      account.campaigns.each  do |campaign|
        temp_value = get_info(campaign)
        if !temp_value.nil?
          @value << temp_value
        end
      end
    rescue Exception => e
      puts e
      return @value
    end
    # this is temporary
    # print_excel_campaigns(@value, account.name)
  end

  def get_info campaign
    begin
      temp_objective = campaign.objective
      if temp_objective == 'PAGE_LIKES' || temp_objective == 'CONVERSIONS' || temp_objective == 'LINK_CLICKS'
        info = cpc_facturation(campaign)
      elsif temp_objective == 'REACH' || temp_objective == 'POST_ENGAGEMENT'
        info = cpm_facturation(campaign)
      end
      return info
    rescue Exception => e
      puts e
      return nil
    end
  end

  def cpm_facturation campaign
    campaign = campaign
    begin
     if !campaign.insights[0].nil?
        value = {nombre: campaign.name, alcance: campaign.insights(fields: 'reach').map(&:reach), cpm: campaign.insights(fields: 'cpm').map(&:cpm),
          impresiones: campaign.insights(fields: 'impressions').map(&:impressions), total_cost: campaign.insights(fields: 'total_action_value').map(&:total_action_value),
          ctr: campaign.insights(fields: 'ctr').map(&:ctr)}
      return value
     end
    rescue Exception => e
      puts  e
      return value
    end
  end

  def cpc_facturation campaign
    campaign = campaign
    begin
      if !campaign.insights[0].nil?
        value =  {nombre: campaign.name, alcance: campaign.insights(fields: 'reach').map(&:reach), cpc: campaign.insights(fields: 'cpc').map(&:cpc),
          impresiones: campaign.insights(fields: 'impressions').map(&:impressions), total_cost: campaign.insights(fields: 'total_action_value').map(&:total_action_value),
          ctr: campaign.insights(fields: 'ctr').map(&:ctr)}
        return value
      end
    rescue Exception => e
      puts  e
      return value
    end
  end

  def cpl_facturation campaign
    campaign = campaign
    begin
      if !campaign.insights[0].nil?
        value = {nombre: campaign.name, alcance: campaign.insights(fields: 'reach').map(&:reach), cpl: campaign.insights(fields: 'cpl').map(&:cpl),
          impresiones: campaign.insights(fields: 'impressions').map(&:impressions), total_cost: campaign.insights(fields: 'total_action_value').map(&:total_action_value),
          ctr: campaign.insights(fields: 'ctr').map(&:ctr)}
        return value
      end
    rescue Exception => e
      puts  e
      return value
    end
  end


  def cpv_facturation campaign
    campaign = campaign
    begin
      if !campaign.insights[0].nil?
        value = {nombre: campaign.name, alcance: campaign.insights(fields: 'reach').map(&:reach), cpv: campaign.insights(fields: 'cpv').map(&:cpv),
                      impresiones: campaign.insights(fields: 'impressions').map(&:impressions), total_cost: campaign.insights(fields: 'total_action_value').map(&:total_action_value),
                      ctr: campaign.insights(fields: 'ctr').map(&:ctr)}
        return value
      end
    rescue Exception => e
      puts  e
      return value
    end
  end

  def cpa_facturation campaign
    campaign = campaign
    begin
      if !campaign.insights[0].nil?
        value = {nombre: campaign.name, alcance: campaign.insights(fields: 'reach').map(&:reach), cpa: campaign.insights(fields: 'cpa').map(&:cpa),
                      impresiones: campaign.insights(fields: 'impressions').map(&:impressions), total_cost: campaign.insights(fields: 'total_action_value').map(&:total_action_value),
                      ctr: campaign.insights(fields: 'ctr').map(&:ctr)}
        return value
      end
    rescue Exception => e
      puts  e
      return value
    end
  end

## Analytics

  # def ruby
  #   @events = @ga.list_events('primary', options: { authorization: user_credentials })
  # end

  def redirect
    client = Signet::OAuth2::Client.new(client_options)

    redirect_to client.authorization_uri.to_s
  end

  def callback
    client = Signet::OAuth2::Client.new(client_options)
    client.code = params[:code]

    response = client.fetch_access_token!
    logger.info(response)
    session[:authorization] = response

    redirect_to '/ruby'
  end

  def ruby
    service = analytics_service
    @account_summaries = service.list_account_summaries
  rescue Google::Apis::AuthorizationError
    @autherror = true
  end

  def analytics
    time = params[:time]
    account = params[:account]
    action = params[:action]
    respond_to do |format|
      case action
        when "Ver información"
          format.html {redirect_to view_analytics_path(account, time)}
        when "Descargar PDF"
          format.html {redirect_to report_path(account, time, :format => :pdf)}
      end
    end
  end

  def view
    @account = params[:id]
    @time = params[:time]
    @ga_convertions = get_convertions.rows
    @ga_convertions = sort_descending(@ga_convertions)
    @ga_cities = get_cities.rows
    @ga_cities = sort_descending(@ga_cities)
    @ga_gender = get_gender.rows
    @ga_gender = sort_descending(@ga_gender)
    @ga_age = get_age.rows
    @ga_age = sort_descending(@ga_age)
  end



  def print_excel
    account = params[:id]
    ga_convertions = sort_descending(get_convertions.rows)
    ga_cities = sort_descending(get_cities.rows)
    ga_gender = sort_descending(get_gender.rows)
    ga_age = sort_descending(get_age.rows)
    excel = Axlsx::Package.new
    name_document = "informe.xlsx"
    excel.workbook.styles do |style|
      wrap_text = style.add_style :alignment => {:horizontal => :left, :vertical => :center, :wrap_text => true}
      excel.workbook.add_worksheet(name: "Conversiones") do |sheet|
        sheet.add_row ["Conversiones", ""]
        if !ga_convertions.nil?
          ga_convertions.each do |conversion|
            sheet.add_row [conversion[0], conversion[1]], style: wrap_text
          end
          sheet.merge_cells "A1:B1"
        end
      end
      excel.workbook.add_worksheet(name: "Ciudad") do |sheet|
        sheet.add_row ["Ciudad", ""]
        if !ga_cities.nil?
          ga_cities.each do |ciudad|
            sheet.add_row [ciudad[0], ciudad[1]], style: wrap_text
          end
          sheet.merge_cells "A1:B1"
        end
      end
      excel.workbook.add_worksheet(name: "Genero") do |sheet|
        sheet.add_row ["Genero", ""]
        if !ga_gender.nil?
          ga_gender.each do |genero|
            sheet.add_row [genero[0], genero[1]], style: wrap_text
          end
          sheet.merge_cells "A1:B1"
        end
      end
      excel.workbook.add_worksheet(name: "Edad") do |sheet|
        sheet.add_row ["Edad", ""]
        if !ga_age.nil?
          ga_age.each do |edad|
            sheet.add_row [edad[0], edad[1]], style: wrap_text
          end
          sheet.merge_cells "A1:B1"
        end
      end
      excel.use_shared_strings = true
      send_data excel.to_stream.read, type: "application/xlsx", filename: name_document
    end
    return
  end

  def get_convertions
    service = analytics_service
    profile_id = 'ga:'+params[:id]
    time = params[:time]
    @analytics = service.get_ga_data(profile_id,time,'today','ga:goalCompletionsAll',{dimensions: "ga:sourceMedium"})
  rescue Google::Apis::AuthorizationError
    redirect
  end

  def get_cities
    service = analytics_service
    profile_id = 'ga:'+params[:id]
    time = params[:time]
    @analytics = service.get_ga_data(profile_id,time,'today','ga:sessions',{dimensions: "ga:city"})
  rescue Google::Apis::AuthorizationError
    redirect
  end

  def get_gender
    service = analytics_service
    profile_id = 'ga:'+params[:id]
    time = params[:time]
    @analytics = service.get_ga_data(profile_id,time,'today','ga:sessions',{dimensions: "ga:userGender"})
  rescue Google::Apis::AuthorizationError
    redirect
  end

  def get_age
    service = analytics_service
    profile_id = 'ga:'+params[:id]
    time = params[:time]
    @analytics = service.get_ga_data(profile_id,time,'today','ga:sessions',{dimensions: "ga:userAgeBracket"})
  rescue Google::Apis::AuthorizationError
    redirect
  end

  def report
    @ga_convertions = get_convertions.rows
    @ga_convertions = sort_descending(@ga_convertions)
    @ga_cities = get_cities.rows
    @ga_cities = sort_descending(@ga_cities)
    @ga_gender = get_gender.rows
    @ga_gender = sort_descending(@ga_gender)
    @ga_age = get_age.rows
    @ga_age = sort_descending(@ga_age)
    date = DateTime.now
    @start_date = get_start_date
    @end_date = date.strftime("%d/%m/%Y")
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "reporte"
      end
    end
  end

  private

  def client_options
    if Rails.env.production?
      redirect_uri = 'https://habla-reports.herokuapp.com/oauth2callback'
    else
      redirect_uri = 'http://localhost:3000/oauth2callback'
    end
    {
      client_id: "267501667920-h1ga3oihuq4nceg2mjhr3akdmhn4v8cg.apps.googleusercontent.com",
      client_secret: "Q4j7i2UnU9hL8jW3HVq7ky7o",
      authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      scope: Google::Apis::AnalyticsV3::AUTH_ANALYTICS_READONLY,
      redirect_uri: redirect_uri
    }
  end

  def analytics_service
    client = Signet::OAuth2::Client.new(client_options)
    client.update!(session[:authorization])
    service = Google::Apis::AnalyticsV3::AnalyticsService.new
    service.authorization = client
    service
  end

  def sort_descending(ga_result)
    ga_result = ga_result.sort_by { |h| h[1].to_i  }.reverse! unless ga_result.nil?
  end

  def get_start_date
    date = DateTime.now
    case params[:time]
    when '30daysAgo'
      start_date = date.last_month.strftime("%d/%m/%Y")
    when '14daysAgo'
      start_date = date.last_week
      start_date = start_date.last_week.strftime("%d/%m/%Y")
    when '7daysAgo'
      start_date = date.last_week.strftime("%d/%m/%Y")
    end
  end
end
