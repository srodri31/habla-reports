Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#redirect'
  get '/ruby', to: 'application#ruby'
  get '/oauth2callback', to: 'application#callback', as: 'callback'
  get '/redirect', to: 'application#redirect', as: 'redirect'

  get '/facebook', to:'application#implementation_fbapi', as: 'implementation_fbapi'
  get '/get_value/:id', to: 'application#get_value', as:'get_value'
  get '/analytics/account/:id/time/:time', to: 'application#view', as:'view_analytics'
  get '/report/:id/time/:time', to: 'application#report', as: 'report'
  get '/print_excel/:id/time/:time', to: 'application#print_excel', as: 'print_excel'

  post '/analytics', to: 'application#analytics', as: 'analytics'

end
